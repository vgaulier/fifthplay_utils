#!/bin/bash
#use it inside project directory
#alias example : .bashrc => alias mkva='~/develop/fifthplay_utils/valid_all.sh'
if [ -z $PLATFORM ] || [ $PLATFORM != "test" ]; then
	echo 'You are not in test mode';
	exit 0;
fi

function test_return {
if [[  $? =~ [^$1] ]]; then
	echo "$2 KO !"
	exit 1
else 
	echo "$2 Ok"
fi
}




echo "Start"
echo "Clean & Build ..."
make clean
make | grep *warning
test_return '0-1' "Build"

echo "Running Tests ..."
make rununittest_TestDebug 2>/dev/null | sed -e '/Failure/,/expected/!d'
test_return 0 "Tests"

echo "Running Valgrind ..." 
make valgrind 2>&1 | sed -e '/[^0a-z] errors/!d' 
test_return 0 "Valgrind"

echo "Running Coverage ..."
make coverage 2>/dev/null | sed -e '/Failure/,/expected/!d'
test_return 0 "Coverage"

echo "End"
