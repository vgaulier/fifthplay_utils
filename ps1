# echo "source ~/develop/utils_fifthplay/ps1" >>  ~/.bashrc

function git_branch () {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
 
function short_path() {
    pwd 2> /dev/null | sed -e 's/\(.*\)\/develop\/embedded\/applications\/\(.*\)/\2/' -e 's/\(.*\)\/develop\/embedded\/applications/APP/' -e 's/\(.*\)\/develop\/yocto\/build\/\(.*\)/BLD\/\2/' -e 's/\(.*\)\/develop\/yocto\/build/BLD/'
}

function export_env() {
    . export_env.sh $1
    PS1='\[\033[00m\]:\[\033[01;34m\]$(short_path)\[\033[0;33m\]($(git_branch)/\[\033[0;31m\]$PLATFORM)\[\033[00m\]\$ '
}

function branch_info() {
    if [[ $(pwd) == *"develop/yocto/build"* ]]
    then
        echo ""
    else
        echo "$(git_branch)/"
    fi
}

function ypc() {
    . ~/develop/yocto/meta-fifthplay/env_yocto.sh -g
     PS1='\[\033[00m\]:\[\033[01;34m\]$(short_path)\[\033[0;33m\]($(branch_info)\[\033[0;31m\]$MACHINE)\[\033[00m\]\$ '
}

function ye() {
    . ~/develop/yocto/meta-fifthplay/env_yocto.sh
     PS1='\[\033[00m\]:\[\033[01;34m\]$(short_path)\[\033[0;33m\]($(branch_info)\[\033[0;31m\]$MACHINE)\[\033[00m\]\$ '
}

